import React from "react";
import { shallow } from "enzyme";
import Rule from "./rule";

describe("#Rule", () => {
  let rule;
  beforeEach(() => {
    rule = {
      id: 1,
      title: "If you don't have a mobile website, you don't have a website.",
      description:
        "In 2014, 50% of worldwide traffic uses mobile. A website must adapt the content for mobile.",
      likes: 0,
      dislikes: 0,
      tags: ["ui"]
    };
  });

  it("should render basic component", () => {
    const wrapper = shallow(<Rule rule={rule} />);
    expect(wrapper).toMatchSnapshot();
  });

  it("should have correct initial state", () => {
    let wrapper = shallow(<Rule rule={rule} />);
    expect(wrapper.state("expanded")).toBe(true);
    delete rule.description;
    wrapper = shallow(<Rule rule={rule} />);
    expect(wrapper.state("expanded")).toBe(false);
  });

  it("should not display body is there is no description", () => {
    delete rule.description;
    const wrapper = shallow(<Rule rule={rule} />);
    expect(wrapper.find(".panel-body")).toHaveLength(0);
  });
});
