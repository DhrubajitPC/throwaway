import React from "react";
import { shallow } from "enzyme";
import RuleList from "./ruleList";
import Rule from "./rule";

describe("#RuleList", () => {
  it("should render basic component", () => {
    const wrapper = shallow(<RuleList />);
    expect(wrapper).toMatchSnapshot();
  });

  it("should render correct number of <Rule />", () => {
    const wrapper = shallow(<RuleList />);
    expect(wrapper.find(Rule)).toHaveLength(4);
  });
});
