import React from "react";
import RuleList from "./components/ruleList";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.css";

ReactDOM.render(<RuleList />, document.getElementById("root"));
