import React, { Component } from "react";
import Rule from "./rule";
import rules from "../data.json";

export default class RuleList extends Component {
  render() {
    return (
      <>
        {rules.map(item => (
          <Rule key={item.id} rule={item} />
        ))}
      </>
    );
  }
}
