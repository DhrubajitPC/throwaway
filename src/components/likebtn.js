import React, { Component } from "react";

export default class LikeBtn extends Component {
  state = {
    count: this.props.count
  };

  handleClick() {
    this.setState(prevState => ({
      count:
        this.props.type === "up" ? prevState.count + 1 : prevState.count - 1
    }));
  }

  render() {
    return (
      <a
        className="btn btn-default"
        title="+1"
        onClick={this.handleClick.bind(this)}
      >
        {this.state.count + " "}
        {this.props.type === "up" ? (
          <i className="glyphicon glyphicon-thumbs-up" />
        ) : (
          <i className="glyphicon glyphicon-thumbs-down" />
        )}
      </a>
    );
  }
}
