import React, { Component } from "react";
import LikeBtn from "./likebtn";

export default class Rule extends Component {
  state = {
    expanded: Boolean(this.props.rule.description)
  };

  handleClick(e) {
    this.setState(prevState => ({
      expanded: !prevState.expanded
    }));
  }

  render() {
    const { description, title, likes, dislikes, tags } = this.props.rule;
    return (
      <div className="panel panel-primary">
        <div className="panel-heading" role="presentation">
          {title}
          <i
            className="pull-right glyphicon glyphicon-chevron-down"
            onClick={this.handleClick.bind(this)}
          />
        </div>
        {this.state.expanded && (
          <div className="panel-body">
            <p>{description}</p>
          </div>
        )}
        <div className="panel-footer">
          <div className="btn-toolbar">
            {tags.map(item => (
              <span className="badge">{item}</span>
            ))}
            <div className="btn-group btn-group-xs pull-right">
              <a className="btn btn-primary" title="Update">
                <i className="glyphicon glyphicon-pencil" />
              </a>
            </div>
            <div className="btn-group btn-group-xs pull-right">
              <LikeBtn type="up" count={likes} />
              <LikeBtn type="down" count={dislikes} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
