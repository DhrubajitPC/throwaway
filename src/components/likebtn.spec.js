import React from "react";
import { shallow } from "enzyme";
import LikeBtn from "./likebtn";

describe("#LikeBtn", () => {
  it("render basic component", () => {
    const wrapper = shallow(<LikeBtn type="up" count={2} />);
    const wrapper2 = shallow(<LikeBtn type="down" count={1} />);
    expect(wrapper).toMatchSnapshot();
    expect(wrapper2).toMatchSnapshot();
  });

  it("should have correct state on render", () => {
    const wrapper = shallow(<LikeBtn type="up" count={2} />);
    expect(wrapper.state("count")).toBe(2);
  });

  it("should increment counter correctly", () => {
    const wrapper = shallow(<LikeBtn type="up" count={2} />);
    wrapper.find("a").simulate("click");
    expect(wrapper.state("count")).toBe(3);
  });

  it("should decrement counter correctly", () => {
    const wrapper = shallow(<LikeBtn type="down" count={2} />);
    wrapper.find("a").simulate("click");
    expect(wrapper.state("count")).toBe(1);
  });
});
